# Rasttny's Server

This page can be used to make Tickets for Issues on the Server, to make Ban Appeals, etc.
When making a ticket or ban appeal, please turn on the Confidential option so only you and Staff can see it, instead of everyone.

Current Modpack: https://www.curseforge.com/minecraft/modpacks/temporal-mechanics-of-creativity

IP: tmc.rnshosting.xyz

Rules:

#1: No cheating, exploiting bugs, hacking, or other illegal activities.

#2: Remember the human! Be nice to your fellow players, hate speech is NOT tolerated under any circumstances.

#3: Do not Grief, or Raid, or steal from anyone.

#4: Farms are allowed, but only when discussed with a Staff member and should be public if they have a lot of lag potential. All farms should also be able to be toggled off.

#5: Make all contraptions togglable.

#6: No trespassing.

#7: No NSFW or Offensive names for your TARDIS or any other items. Also no offensive skins.

#8: Cut down the whole tree.


To donate to the server, use this link:
https://www.paypal.me/newtardismodserver
Donation tiers are like this:

£1 = Discord Donator Role (Includes access to #donators-only channel)

£2.50 = In-Game Donator Role + The Above

£5 = In-Game Custom Role + The Above

Staff Members:

Owner: Rasttny

Senior Head Admin: Destroyer021620

Senior Head Admin: Schwift_

Head Admin: DangerousOnes

Admin: DizzyBright

Admin: Grain_

Mod: DocSaraHunter

Mod: Proscreamerz

Trainee: Halex
